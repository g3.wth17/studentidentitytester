﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PruebaIdentityStudentMentor.Models;
using PruebaIdentityStudentMentor.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PruebaIdentityStudentMentor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : Controller
    {
        private IStudentService service;
        public StudentsController(IStudentService service)
        {
            this.service = service;
        }

        //GetStudents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Student>>> GetStudents()
        {
            try
            {
                return Ok(await service.getStudents());
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudent(string id)
        {
            try
            {
                return Ok(await service.getStudent(id));
            }
            catch (Exception)
            {

                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(Student student)
        {
            try
            {
                return Ok(await service.addStudent(student));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
