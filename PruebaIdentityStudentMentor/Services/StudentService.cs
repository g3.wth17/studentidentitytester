﻿using AutoMapper;
using PruebaIdentityStudentMentor.Data.Entity;
using PruebaIdentityStudentMentor.Data.Repository;
using PruebaIdentityStudentMentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIdentityStudentMentor.Services
{
    public class StudentService : IStudentService
    {
        private IStudentRepository repository;
        private readonly IMapper mapper;
        public StudentService(IStudentRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;

        }
        public async Task<Student> addStudent(Student student)
        {
            var studentEntity = mapper.Map<StudentEntity>(student);
            repository.addStudent(studentEntity);
            if (await repository.SaveChangesAsync())
                return mapper.Map <Student> (studentEntity);
            throw new Exception("There was an error with the DB");
        }

        public async Task<Student> getStudent(string id)
        {
            var studentEntity = await repository.getStudent(id);
            if (studentEntity == null)
                throw new Exception("Not found in DB");
            return mapper.Map<Student>(studentEntity);
        }

        public async Task<IEnumerable<Student>> getStudents()
        {
            var studentEntities = await repository.getStudents();
            return mapper.Map<IEnumerable<Student>>(studentEntities);
        }
    }
}
