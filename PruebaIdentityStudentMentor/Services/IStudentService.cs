﻿using PruebaIdentityStudentMentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIdentityStudentMentor.Services
{
    public interface IStudentService
    {
        Task<IEnumerable<Student>> getStudents();
        Task<Student> getStudent(string id);
        Task<Student> addStudent(Student student);
    }
}
