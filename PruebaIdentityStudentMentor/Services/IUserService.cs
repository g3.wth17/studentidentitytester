﻿using PruebaIdentityStudentMentor.Auth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIdentityStudentMentor.Services
{
    public interface IUserService
    {
        Task<UserManagerResponse> RegisterUserAsync(RegisterView model);
        Task<UserManagerResponse> CreateRoleAsync(CreateRoleView model);
        Task<UserManagerResponse> LoginUserAsync(LoginView model);
        Task<UserManagerResponse> CreateUserRoleAsync(CreateUserRoleViewModel model);
    }
}
