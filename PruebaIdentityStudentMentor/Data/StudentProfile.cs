﻿using AutoMapper;
using PruebaIdentityStudentMentor.Data.Entity;
using PruebaIdentityStudentMentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIdentityStudentMentor.Data
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            this.CreateMap<StudentEntity, Student>().ReverseMap();
        }
    }
}
