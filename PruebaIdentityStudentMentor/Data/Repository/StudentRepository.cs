﻿using Microsoft.EntityFrameworkCore;
using PruebaIdentityStudentMentor.Data.Entity;
using PruebaIdentityStudentMentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIdentityStudentMentor.Data.Repository
{
    public class StudentRepository : IStudentRepository
    {
        private AppDbContext dbContext;
        List<Student> students;
        public StudentRepository(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public void addStudent(StudentEntity student)
        {
            dbContext.Add(student);
        }

        public async Task<StudentEntity> getStudent(string id)
        {
            IQueryable<StudentEntity> query = dbContext.Students;
            query = query.AsNoTracking();
            return await query.SingleOrDefaultAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<StudentEntity>> getStudents()
        {
            IQueryable<StudentEntity> query = dbContext.Students;
            query = query.AsNoTracking();
            return await query.ToArrayAsync();

        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await dbContext.SaveChangesAsync()) > 0;
        }
    }
}
