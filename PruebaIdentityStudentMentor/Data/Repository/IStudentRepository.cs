﻿using PruebaIdentityStudentMentor.Data.Entity;
using PruebaIdentityStudentMentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIdentityStudentMentor.Data.Repository
{
    public interface IStudentRepository
    {
        Task<bool> SaveChangesAsync();
        Task<IEnumerable<StudentEntity>> getStudents();
        Task<StudentEntity> getStudent(string id);
        void addStudent(StudentEntity student);
    }
}
