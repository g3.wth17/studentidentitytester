﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaIdentityStudentMentor.Models
{
    public class Student
    {
        [Required]
        public string Id { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no names that long")]
        public string Name { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no lastnames that long")]
        public string LastName { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no Schools with a name that long")]
        public string School { get; set; }
    }
}
